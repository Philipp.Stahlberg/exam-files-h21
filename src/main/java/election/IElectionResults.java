package election;

import java.util.Set;

/**
 * This class holds a set of voting results, either from partial count or final count
 * The votes can be from a region or all regions combined. 
 * 
 * @author Martin Vatshelle
 */
public interface IElectionResults {

	/**
	 * @return total number of votes in this result
	 */
	public int totalVotes();

	/**
	 * This method counts how many of the votes was for a given party
	 * 
	 * @param party the party which you want number of votes for
	 * @return the number of votes given party got
	 */
	public int countVotes(Party party);

	/**
	 * The Norwegian "Sperregrense" is at 4%, getting at least 4% of the votes will
	 * provide significantly more seats in parliament than getting less than 4%.
	 * 
	 * @param party - the party to check for.
	 * @return true if party has above or equal to 4% of the votes, false otherwise
	 */
	public boolean aboveSperreGrense(Party party);

	/**
	 * This method checks whether or not a coalition of parties got more than half the votes.
	 * In reality what matters is how many representatives a party get, but for simplicity sake in this exam we
	 * will only check for number of votes.
	 * 
	 * @param coalision - the group of parties you want to check for
	 * @return true if the combined votes for the coalition is larger than half the votes,
	 * 			if number of votes is <= half the votes return false;
	 */
	public boolean majority(Set<Party> coalition);

}
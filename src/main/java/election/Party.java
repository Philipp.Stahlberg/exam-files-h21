package election;

/**
 * This Enum lists the biggest Partiesin the Norwegian
 * election of 2021.
 * 
 * @author Martin Vatshelle
 */
public enum Party {
AP, HØYRE, SP, FRP,
SV, RØDT, VENSTRE, MDG, KRF;

}
